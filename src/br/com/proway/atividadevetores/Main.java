package br.com.proway.atividadevetores;

import br.com.proway.atividadevetores.model.Cachorro;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        Cachorro[] cachorros = new Cachorro[3];

        for (int i = 0; i < cachorros.length; i++){
            cachorros[i] = new Cachorro();
            cachorros[i].id = i;
            cachorros[i].nome = JOptionPane.showInputDialog(null, "Informe o nome:");
            cachorros[i].raca =JOptionPane.showInputDialog(null, "Informe a raça:");
        }

        for (int i = 0; i < cachorros.length; i++){
            JOptionPane.showMessageDialog(null, "Id: " + cachorros[i].id + " Nome: " + cachorros[i].nome + " Raça: " + cachorros[i].raca);
        }
    }
}
